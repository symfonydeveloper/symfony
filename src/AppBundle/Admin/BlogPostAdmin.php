<?php
/**
 * Created by PhpStorm.
 * User: bc120401339
 * Date: 11/11/2016
 * Time: 12:32 PM
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BlogPostAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', 'text')
            ->add('body', 'textarea')
            ->add('category', 'entity', array(
                'class' => 'AppBundle\Entity\Category',
                'property' => 'name',
            ));

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        // ... configure $listMapper
    }
}