<?php
/**
 * Created by PhpStorm.
 * User: bc120401339
 * Date: 11/9/2016
 * Time: 4:32 PM
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HomeController extends Controller
{
    /**
     * @Route("home", name="home")
     */
    public function homeAction (Request $request)
    {
       return $this->render('home/home.html.twig');
    }
}