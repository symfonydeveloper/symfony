<?php
///**
// * Created by PhpStorm.
// * User: bc120401339
// * Date: 11/11/2016
// * Time: 3:12 PM
// */
//
//namespace AppBundle\Controller\SampleController;
//
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
//use AppBundle\Entity\User;
//use Symfony\Component\HttpFoundation\Request;
//class WebServices extends FOSUser
//{
//    /**
//     * @Rest\Post("/user/")
//     */
//    public function postAction(Request $request)
//    {
//        $data = new User;
//        $firstname = $request->get('first name');
//        $lastname = $request->get('last name');
//        $username = $request->get('username');
//        $emailaddress = $request->get('email address');
//        $workaddress = $request->get('work address');
//        $postalcode = $request->get('postal code');
//
//        if(empty($firstname) || empty($lastname) || empty($username) || empty($emailaddress ) || empty($workaddress) || empty($postalcode))
//        {
//            return response()->json(array("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE));
//        }
//        $data->setFirstname($firstname);
//        $data->setLastname($lastname);
//        $data->setRole($username );
//        $data->setEmailaddress($emailaddress);
//        $data->setWorkaddress($workaddress);
//        $data->setPostalcode($postalcode);
//        $em = $this->getDoctrine()->getManager();
//        $em->persist($data);
//        $em->flush();
//        return $em->json(array("User Added Successfully", Response::HTTP_OK));
//    }
//}